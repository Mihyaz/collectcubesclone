﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageReader : MonoBehaviour
{
    [SerializeField]
    private Texture2D[] _images;
    private Texture2D _image;

    [Header("Data")]
    public List<CubePositionData> CubePositionDatas = new List<CubePositionData>();

    public void GenerateAll()
    {
        for (int i = 0; i < CubePositionDatas.Count; i++)
        {
            Generator(i);
        }
    }

    public void GenerateWithLevelID(int ID)
    {
        CubePositionDatas[ID].CollectableDatas.Clear();
        CubePositionDatas[ID].CubeCount = 0;

        Generator(ID);
    }

    public void ResetAll()
    {
        Resetor();
    }

    public void ResetWithLevelID(int ID)
    {
        CubePositionDatas[ID].CollectableDatas.Clear();
        CubePositionDatas[ID].CubeCount = 0;
    }

    private void Generator(int index)
    {
        _image = _images[index];
        Color[] pix = _image.GetPixels();

        int worldX = _image.width;
        int worldZ = _image.height;

        Vector3[] spawnPositions = new Vector3[pix.Length];
        Vector3 startingSpawnPosition = new Vector3(-Mathf.Round(worldX / 2), 0, -Mathf.Round(worldZ / 2));
        Vector3 currentSpawnPos = startingSpawnPosition;

        int counter = 0;

        for (int z = 0; z < worldZ; z++)
        {
            for (int x = 0; x < worldX; x++)
            {
                spawnPositions[counter] = currentSpawnPos;
                counter++;
                currentSpawnPos.x++;
            }

            currentSpawnPos.x = startingSpawnPosition.x;
            currentSpawnPos.z++;
        }

        counter = 0;
        int cubeCounter = 0;

        foreach (Vector3 pos in spawnPositions)
        {
            Color c = pix[counter];
            if (c.Equals(Color.white)) // For handling colorful pixel art.
            {
                CubePositionDatas[index].CollectableDatas.Add(new CollectableData(pos));
                cubeCounter++;
            }
            counter++;
        }
        CubePositionDatas[index].CubeCount = cubeCounter;

    }

    public void Resetor()
    {
        for (int i = 0; i < CubePositionDatas.Count; i++)
        {
            CubePositionDatas[i].CollectableDatas.Clear();
            CubePositionDatas[i].CubeCount = 0;
        }
    }

}
