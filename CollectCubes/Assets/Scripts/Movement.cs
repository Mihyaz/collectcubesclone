﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour, IMove
{
    public Rigidbody Rigidbody { get ; set; }
    public Transform Transform { get ; set ; }
    
    [Range(1, 10)]
    public int Speed;

    public FloatingJoystick _floatingJoystick;
    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        Transform = transform; //Caching transform is about %10-15 performance improvement as I checked on Profiler

        Speed = 4;
    }

    public void Move()
    {
        Rigidbody.velocity = new Vector3(_floatingJoystick.Horizontal * Speed, Rigidbody.velocity.y, _floatingJoystick.Vertical * Speed);
        
        if (_floatingJoystick.Horizontal != 0f || _floatingJoystick.Vertical != 0f)
        {
            Rigidbody.rotation = Quaternion.LookRotation(-Rigidbody.velocity / 2);
        }
    }
}
