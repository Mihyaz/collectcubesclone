﻿using System;
using UnityEngine;

public interface IMove
{
    Rigidbody Rigidbody { get; set; }
    Transform Transform { get; set; }

    void Move();
}