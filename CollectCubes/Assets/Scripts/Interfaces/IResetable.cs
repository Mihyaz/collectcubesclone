﻿public interface IResetable
{
    void ResetThis();
}