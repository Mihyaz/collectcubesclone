﻿using UnityEngine;

public class CaseStudy
{
    public static void Message()
    {
        Time.timeScale = 0;
        Debug.Log("<color=red>End of the case study...</color>");
        Debug.Log("Thank you, <color=Green> Alictus</color>");
        UnityEditor.EditorApplication.isPlaying = false;
    }
}
