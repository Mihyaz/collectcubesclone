﻿using UnityEngine;
using System.Collections;
using Zenject;
using System.Collections.Generic;
using System.IO.IsolatedStorage;

public class CollectablePool : MonoBehaviour
{
    [Inject]
    private LevelManager _levelManager;

    [SerializeField] List<Cube> _cubes  = new List<Cube>();

    private void Awake()
    {
        _levelManager.OnLevelCompleted += DisableCollectables;
        _levelManager.OnRetryLevel     += DisableCollectables;

        _levelManager.OnNewLevelStart  += Generator;
        _levelManager.OnRetryLevel     += Generator;
    }

    private void Start()
    {
        Generator();
    }

    public Cube GetPooledCollectable()
    {
        for (int i = 0; i < _cubes.Count; i++)
        {
            if (!_cubes[i].gameObject.activeInHierarchy)
            {
                return _cubes[i];
            }
        }
        return null;
    }

    private void ActivateCollectable(int index)
    {
        Cube Collectable = GetPooledCollectable();
        if (Collectable != null)
        {
            Collectable.gameObject.SetActive(true);
            Collectable.Transform.localPosition = _levelManager.GetCubePosition(index);
            Collectable.Transform.localRotation = Quaternion.identity;
        }
    }

    private void DisableCollectables()
    {
        //Do particle effects or recursive disabler with delay

        for (int i = 0; i < _levelManager.GetCubeCount(); i++)
        {
            if(_cubes[i].gameObject.activeInHierarchy)
            {
                _cubes[i].gameObject.SetActive(false);
            }
        }
    }

    private void Generator()
    {
        for (int i = 0; i < _levelManager.GetCubeCount(); i++)
        {
            ActivateCollectable(i);
        }
    }

}
