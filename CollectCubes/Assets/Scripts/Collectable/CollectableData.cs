﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CollectableData
{
    public Vector3 Position;

    public CollectableData(Vector3 pos)
    {
        Position = pos;
    }
}
