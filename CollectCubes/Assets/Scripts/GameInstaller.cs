using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] LevelManager _levelManager;
    [SerializeField] UIManager    _uiManager;
    public override void InstallBindings()
    {
        Container.Bind<LevelManager>().FromInstance(_levelManager);
        Container.Bind<UIManager>().FromInstance(_uiManager);
    }
}