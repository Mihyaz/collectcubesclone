﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "CubePositionsData", menuName = "ScriptableObjects/CubePositionsData", order = 1)]
public class CubePositionData : ScriptableObject
{
    public int Level_ID;
    public int CubeCount;
    public List<CollectableData> CollectableDatas = new List<CollectableData>();
}
