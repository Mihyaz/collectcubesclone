﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(fileName = "ColorData", menuName = "ScriptableObjects/ColorData", order = 2)]
public class ColorData : ScriptableObject
{
    public ColorPalette White;
    public ColorPalette Orange;

    [Serializable]
    public struct ColorPalette
    {
        public Color MapColor;
        public Color EmissionColor;
    }
}
