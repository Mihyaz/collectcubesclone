﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Zenject;
using System;

public class Vortex : MonoBehaviour
{
    [Inject] private LevelManager _levelManager;
    [Inject] private UIManager _uiManager;

    [SerializeField] SphereCollider _collider;

    private int _score;

    private void Awake()
    {
        _levelManager.OnLevelCompleted += ResetThis;
    }

    private void ResetThis()
    {
        _score = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Cube cube)) //This is huge trick prevent GC allocation
        {
            cube.Rigidbody.AddForce((transform.localPosition - cube.transform.position) * 2f);
            cube.ChangeMaterial();

            PhysicLayers.SwitchToIgnored(cube.gameObject);
            ProgressHandler();
        }
    }

    private void ProgressHandler()
    {
        _score++;

        if (_score >= _levelManager.GetCubeCount())
        {
            _levelManager.LevelCompleted();
            return;
        }

        _uiManager.LevelProgression.FillImage(1f / _levelManager.GetCubeCount());

    }

}
