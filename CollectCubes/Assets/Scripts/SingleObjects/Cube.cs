﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{

    [HideInInspector] public Rigidbody Rigidbody;
    [HideInInspector] public Material  Material;
    [HideInInspector] public Transform Transform;

    [SerializeField] private ColorData ColorData;

    public void ChangeMaterial()
    {
        Material.SetColor("_EmissionColor", ColorData.Orange.EmissionColor);
        Material.color = ColorData.Orange.MapColor;
    }

    private void Awake()
    {
        ColorData = Resources.Load<ColorData>("ColorDatas/ColorData");

        Material = GetComponent<MeshRenderer>().material;
        Rigidbody = GetComponent<Rigidbody>();
        Transform = transform;
    }

    private void Start()
    {
        ResetThis();
    }

    private void OnDisable()
    {
        ResetThis();
    }

    private void ResetThis()
    {
        //Reason I toggle rb kinematics is to prevent collider explosion on spawn
        Rigidbody.isKinematic = true;
        Material.SetColor("_EmissionColor", ColorData.White.EmissionColor);
        Material.color = ColorData.White.MapColor;
        gameObject.layer = (int)Layers.Cubes;
        Rigidbody.isKinematic = false;
    }

}
