﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Zenject;

public class Collector : MonoBehaviour
{
    [Inject]
    private LevelManager _levelManager;

    private IMove _movement;
    private VectorComponents _positions;

    private void Awake()
    {
        _movement = GetComponent<IMove>();

        _levelManager.OnNewLevelStart  += ResetPosition;
        _levelManager.OnNewLevelStart  += ScriptToggler;

        _levelManager.OnRetryLevel     += ResetPosition;

        _levelManager.OnLevelCompleted += ScriptToggler;
    }

    private void Start()
    {
        _positions = new VectorComponents
        {
            InitialPosition = _movement.Transform.localPosition,
            InitialRotation = _movement.Transform.localEulerAngles
        };
    }

    public void FixedUpdate()
    {
        _movement.Move();
    }

    private void ResetPosition()
    {
        _movement.Transform.localPosition    = _positions.InitialPosition;
        _movement.Transform.localEulerAngles = _positions.InitialRotation;
    }

    private void ScriptToggler()
    {
        enabled = !enabled;
    }

    public struct VectorComponents
    {
        public Vector3 InitialPosition;
        public Vector3 InitialRotation;
    }
}
