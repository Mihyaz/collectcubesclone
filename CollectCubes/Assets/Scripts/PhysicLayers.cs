﻿using UnityEngine;
public enum Layers
{
    Collector = 8,
    Vortex    = 9,
    Cubes     = 10,
    Map       = 11,
    Ignored   = 12
}

public class PhysicLayers : Physics
{
    public static void SwitchToIgnored(GameObject gameObject)
    {
        gameObject.layer = (int)Layers.Ignored;
    }
}
