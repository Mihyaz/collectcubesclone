﻿using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using OnurMihyaz;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    [Inject] private LevelManager _levelManager;
    public LevelProgression LevelProgression;
    public LevelCompletedScreen LevelCompletedScreen;

    private List<IResetable> _resetables = new List<IResetable>(); 

    private void Awake()
    {
        _levelManager.OnLevelCompleted += UpdateLevelCompletedScreen;
        _levelManager.OnNewLevelStart  += ResetUI;
        _levelManager.OnRetryLevel     += ResetUI;

        LevelProgression.UpdateLevelTexts(_levelManager.Level); //Probably this inits with save/load data

    }

    private void Start()
    {
        _resetables.Add(LevelProgression);
        _resetables.Add(LevelCompletedScreen);
    }

    private void ResetUI()
    {       
        for (int i = 0; i < _resetables.Count; i++) 
            _resetables[i].ResetThis();

        LevelProgression.UpdateLevelTexts(_levelManager.Level);
    }

    private void UpdateLevelCompletedScreen()
    {
        LevelCompletedScreen.Announcer.text = "Level " + (_levelManager.Level + 1) + " Completed";
        LevelCompletedScreen.DOCelebration();


        StartCoroutine(MihyazDelay.Delay(1, () =>
        {
            LevelCompletedScreen.Dimmer.DOFade(1, 2f).OnComplete(() =>
            {
                _levelManager.StartNewLevel();
                LevelCompletedScreen.Dimmer.DOFade(0, 2f).SetDelay(0.25f);
            });
        }));
    }
}

[Serializable]
public class LevelProgression : IResetable
{
    public TextMeshProUGUI CurrentLevel;
    public TextMeshProUGUI NextLevel;

    public Image ProgressBar;

    public void FillImage(float amount)
    {
        ProgressBar.fillAmount += amount;
    }

    public void UpdateLevelTexts(int level)
    {
        level += 1;
        CurrentLevel.text =  level.ToString();
        NextLevel.text  = (level + 1).ToString();
    }

    public void ResetThis()
    {
        ProgressBar.fillAmount = 0;
    }
}

[Serializable]
public class LevelCompletedScreen : IResetable
{
    public GameObject Confetti;
    public TextMeshProUGUI Announcer;
    public Image Dimmer;

    public void DOCelebration()
    {
        Confetti.SetActive(true);

        Announcer.transform.localScale = Vector2.zero;
        Announcer.transform.DOScale(1.5f, 0.25f).OnComplete(() => 
        {
            Announcer.transform.DOScale(1, 0.15f);
        });
    }

    public void ResetThis()
    {
        Announcer.transform.localScale = Vector2.zero;
    }
}
