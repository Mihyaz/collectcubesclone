﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using OnurMihyaz;

public class LevelManager : MonoBehaviour
{
    // Maybe I could inject this scriptableObject
    [Header("Data")]
    public List<CubePositionData> CubePositionDatas;

    public int Level;

    public event Action OnNewLevelStart;
    public event Action OnLevelCompleted;
    public event Action OnRetryLevel;

    public void StartNewLevel()
    {
        Level++;
        if (Level >= 3)
        {
            CaseStudy.Message();
        }
        OnNewLevelStart?.Invoke();
    }

    public void LevelCompleted()
    {
        OnLevelCompleted?.Invoke();
    }

    public void LevelRetry()
    {
        OnRetryLevel?.Invoke();
    }
        
    public int GetCubeCount() => CubePositionDatas[Level].CubeCount;
    public Vector3 GetCubePosition(int index) => CubePositionDatas[Level].CollectableDatas[index].Position;
}
