﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class GameEditor : EditorWindow
{
    private Label m_Label;
    private IntegerField m_IntField;
    private Button m_GenerateAll;
    private Button m_GenerateWithID;
    private Button m_ResetAll;
    private Button m_ResetWithID;

    [Header("Data")]
    public List<CubePositionData> CubePositionDatas;

    [MenuItem("Window/GameEditor")]
    public static void ShowWindow()
    {
        GetWindow<GameEditor>("Game Editor");
    }

    public void OnEnable()
    {
        var root = this.rootVisualElement;

        #region Label
        m_Label = new Label()
        {
            text = "Level Editor"
        };
        m_Label.style.fontSize = 20f;
        m_Label.style.color = new Color(0.41f, 1f, 0.47f);

        #endregion

        #region Generate All Section
        m_GenerateAll = new Button();

        m_GenerateAll.text = "Generate All";
        m_GenerateAll.clicked += Generate;
        #endregion
        #region ResetAll Section
        m_ResetAll = new Button();

        m_ResetAll.text = "Reset All";
        m_ResetAll.clicked += ResetAll;
        #endregion

        #region Generate With ID Section
        m_IntField = new IntegerField();
        m_IntField.style.maxWidth = 70;

        m_GenerateWithID = new Button();
        m_GenerateWithID.text = "Generate With ID";
        m_GenerateWithID.clicked += GenerateWithID;
        #endregion

        #region Reset With ID Section
        m_IntField = new IntegerField();
        m_IntField.style.maxWidth = 70;

        m_ResetWithID = new Button();
        m_ResetWithID.text = "Reset With ID";
        m_ResetWithID.clicked += ResetWithID;
        #endregion

        root.Add(m_Label);
        root.Add(m_IntField);
        root.Add(m_GenerateWithID);
        root.Add(m_GenerateAll);
        root.Add(m_ResetAll);
        root.Add(m_ResetWithID);

        Bind();
    }

    private void Generate()
    {
        FindObjectOfType<ImageReader>().GenerateAll();
        for (int i = 0; i < FindObjectOfType<ImageReader>().CubePositionDatas.Count; i++)
        {
            EditorUtility.SetDirty(FindObjectOfType<ImageReader>().CubePositionDatas[i]);
        }
    }

    private void GenerateWithID()
    {
        FindObjectOfType<ImageReader>().GenerateWithLevelID(int.Parse(m_IntField.text));
        EditorUtility.SetDirty(FindObjectOfType<ImageReader>().CubePositionDatas[int.Parse(m_IntField.text)]);
    }

    private void ResetAll()
    {
        FindObjectOfType<ImageReader>().ResetAll();
        for (int i = 0; i < FindObjectOfType<ImageReader>().CubePositionDatas.Count; i++)
        {
            EditorUtility.SetDirty(FindObjectOfType<ImageReader>().CubePositionDatas[i]);
        }
    }

    private void ResetWithID()
    {
        FindObjectOfType<ImageReader>().ResetWithLevelID(int.Parse(m_IntField.text));
        EditorUtility.SetDirty(FindObjectOfType<ImageReader>().CubePositionDatas[int.Parse(m_IntField.text)]);

    }

    private void Bind()
    {
        //SerializedObject so = new SerializedObject(FindObjectOfType<GameManager>());
        //m_IntField.Bind(so);
    }
}
